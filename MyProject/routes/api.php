<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', 'UsersController@apiUsers');

Route::get('/user/id/{id}', 'UsersController@apiGetUserById');

Route::get('/user/username/{username}', 'UsersController@apiGetUserByUsername');

Route::post('/user', 'UsersController@apiCreate');

Route::delete('/user/id/{id}', 'UsersController@apiDeleteUserById');

Route::delete('/user/username/{username}', 'UsersController@apiDeleteUserByUsername');

Route::put('/user/id/{id}', 'UsersController@apiUpdateUserById');

Route::put('/user/username/{username}', 'UsersController@apiUpdateUserByUsername');


Route::get('/blogs', 'BlogController@index');

Route::get('/blog/{id}', 'BlogController@show');

Route::post('/blog', 'BlogController@store');

Route::delete('/blog/{id}', 'BlogController@destroy');

Route::put('/blog/{id}', 'BlogController@update');


Route::get('/comments', 'CommentsController@index');

Route::get('/comments/{id}', 'CommentsController@show');

Route::post('comments', 'CommentsController@store');

Route::delete('/comments/{id}', 'CommentsController@destroy');

Route::put('/comments/{id}', 'CommentsController@update');


