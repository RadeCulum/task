<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UsersController@webIndex');

Route::get('/user/add', 'UsersController@webAddIndex');

Route::get('/user/update', 'UsersController@webUpdateIndex');

Route::get('/user/delete', 'UsersController@webDeleteIndex');


Route::post('/user/add', 'UsersController@webAddUser');

Route::post('/user/update', 'UsersController@webUpdateUser');

Route::post('/user/delete', 'UsersController@webDeleteUser');


