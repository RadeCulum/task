<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = ['title', 'content', 'user_id'];

    public static  function getBlogById($id){
        return Blog::where('id', $id)->first();
    }

    public static function  createBlog($title, $content, $user_id){
        $blog = new Blog();

        $blog->title  = $title;
        $blog->content = $content;
        $blog->user_id = $user_id;

        return $blog;
    }

    public static function  createAndSaveBlog($title, $content, $user_id){
        $blog = new Blog();

        $blog->title  = $title;
        $blog->content = $content;
        $blog->user_id = $user_id;

        $blog->save();

        return $blog;
    }

    public static function  deleteBlogById($id){
        Blog::where('id', $id)->first()->delete();
    }

    public static  function updateBlogById($id, $newBlog){
        DB::table('blog')
            ->where('id', $id)
            ->update(array(
                'title' => $newBlog->title,
                'content' => $newBlog->content,
                'user_id' => $newBlog->user_id
            ));
    }
}
