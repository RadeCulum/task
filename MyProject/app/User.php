<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    protected $table = 'user';

    protected $fillable = ['first_name', 'last_name', 'username'];


    public static  function getUserById($id){
        return User::where('id', $id)->first();
    }

    public static function  getUserByUsername($username){
        return User::where('username', $username)->first();
    }

    public static function  createUser($first_name, $last_name, $username){
        $user = new User();

        $user->first_name  = $first_name;
        $user->last_name = $last_name;
        $user->username = $username;

        return $user;
    }

    public static function  createAndSaveUser($first_name, $last_name, $username){
        $user = new User();

        $user->first_name  = $first_name;
        $user->last_name = $last_name;
        $user->username = $username;

        $user->save();

        return $user;
    }

    public static function  deleteUserById($id){
        User::where('id', $id)->first()->delete();
    }

    public static function  deleteUserByUsername($username){
        User::where('username', $username)->first()->delete();
    }

    public static  function updateUserById($id, $newUser){
        DB::table('user')
            ->where('id', $id)
            ->update(array(
                'first_name' => $newUser->first_name,
                'last_name' => $newUser->last_name,
                'username' => $newUser->username
            ));
    }

    public static  function updateUserByusername($username, $newUser){
        DB::table('user')
            ->where('username', $username)
            ->update(array(
                'first_name' => $newUser->first_name,
                'last_name' => $newUser->last_name,
                'username' => $newUser->username
            ));
    }

}
