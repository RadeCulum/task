<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

use App\Comments;
use App\Blog;
class CommentsController extends Controller
{
    public function index() {
        try{
            return response()->json([Comments::all()])
                ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        }catch(Exception $exception){
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function show($id) {
        try {
            $comments = Comments::getCommentById($id);
            if(is_null($comments)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                return response()->json($comments)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }

        }catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function store(Request $request) {
        try {
            $nickname = $request->input('nickname');
            $text = $request->input('text');
            $blog_id = $request->input('blog_id');

            if($this->isAnyNullOrEmpty( array($nickname, $text, $blog_id))) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
            } else {
                $comments = Comments::createAdnSaveComment($nickname, $text, $blog_id);
                return response()->json($comments)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function destroy($id) {
        try {
            $comments = Comments::getCommentById($id);

            if (is_null($comments)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                Comments::deleteCommentById($id);

                return response()->json($comments)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch (Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function update(Request $request, $id) {
        try{
            $comments = Comments::getCommentById();
            if(is_null($comments)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                 $nickname = request('nickname', $comments->nickname);
                 $text = request('text', $comments->text);
                 $blog_id = request('blog_id', $comments->blog_id);

                 $new_comment = Comments::createComment($nickname, $text, $blog_id);
                 Comments::updateCommentById($id, $new_comment);

                return response()->json(Comments::where('id', $id)->first())
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    private function isAnyNullOrEmpty(array $array){
        foreach ($array as $item) {
            if(is_null($item) || empty($item)) {
                return true;
            }
        }
        return false;
    }
}
