<?php

namespace App\Http\Controllers;

use App\Blog;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index() {
        try{
            return response()->json([ Blog::all()])
                ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        }catch(Exception $exception){
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function store(Request $request) {
        try {
            $title = $request->input('title');
            $content = $request->input('content');
            $user_id = $request->input('user_id');

            if($this->isAnyNullOrEmpty(array($title, $content, $user_id))) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
            } else {
                $blog = Blog::createAndSaveBlog($title, $content, $user_id);
                return response()->json($blog)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }


    public function show($id) {
        try {
            $blog = Blog::getBlogById($id);
            if(is_null($blog)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                return response()->json($blog)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }

        }catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function update(Request $request, $id) {
        try{
            $blog = Blog::getBlogById($id);
            if(is_null($blog)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            }
             else {
                 $title = request('title', $blog->title);
                 $content = request('content', $blog->content);
                 $user_id = request('user_id', $blog->user_id);

                 $new_blog = Blog::createBlog($title, $content, $user_id);
                 Blog::updateBlogById($id, $new_blog);

                return response()->json(Blog::where('id', $id)->first())
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }


    public function destroy($id) {
        try {
            $blog = Blog::getBlogById($id);

            if (is_null($blog)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                Blog::deleteBlogById($id);

                return response()->json($blog)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch (Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    private function isAnyNullOrEmpty(array $array){
        foreach ($array as $item) {
            if(is_null($item) || empty($item)) {
                return true;
            }
        }
        return false;
    }
}
