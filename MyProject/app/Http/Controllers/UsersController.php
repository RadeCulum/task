<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function webIndex(){

        $users = User::all();

        return view('users.index',compact('users'));
    }

    public function webAddIndex(){
        return view('users.add');
    }

    public function webDeleteIndex(){
        return view('users.delete');
    }

    public function webUpdateIndex(){
        return view('users.update');
    }

    public function webUpdateUser(){

        $old_username = request('old_username');
        $new_first_name = request('first_name');
        $new_last_name = request('last_name');
        $new_username = request('new_username');

        DB::table('user')
            ->where('username', $old_username)
            ->update([
                'first_name' => $new_first_name,
                'last_name' => $new_last_name,
                'username' => $new_username]);

        return redirect('/users');
    }

    public function webDeleteUser(){
        $username = request('username');

        DB::table('user')
            ->where('username', $username)
            ->delete();
        return redirect('/users');
    }

    public function webAddUser(){

        $user = new User();
        $user->first_name = request("first_name");
        $user->last_name = request("last_name");
        $user->username = request("username");
        $user->save();

        return redirect('/users');

    }



    public function apiUsers() {
        try{
            return response()->json([User::all()])
                ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
        }catch(Exception $exception){
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function apiGetUserById($id) {
        try {
            $user = User::getUserById($id);
            if(is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                return response()->json($user)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }

        }catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }

    }

    public function apiGetUserByUsername($username) {
        try {
            $user = User::getUserByUsername($username);
            if(is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            }  else {
                return response()->json($user)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function apiCreate(Request $request) {
        try {
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $username = $request->input('username');

            $user = User::getUserByUsername($username);

            if(!is_null($user) || $this->isAnyNullOrEmty(array($first_name, $last_name, $username))) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_BAD_REQUEST, Response::$statusTexts[Response::HTTP_BAD_REQUEST]);
            } else {
                $user = User::createAndSaveUser($first_name, $last_name, $username);
                return response()->json($user)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function apiDeleteUserById($id) {
        try {
            $user = User::getUserById($id);

            if (is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                User::deleteUserById($id);

                return response()->json($user)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch (Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function apiDeleteUserByUsername($username){
        try {
            $user = User::getUserById($username);

            if (is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            } else {
                User::getUserByUsername($username);

                return response()->json($user)
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function apiUpdateUserById(Request $request, $id){
        try{
            $user = User::getUserById($id);
            if(is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            }
             else {
                 $first_name = request('first_name', $user->first_name);
                 $last_name = request('last_name', $user->last_name);
                 $new_username = request('username', $user->username);

                 $new_user = User::createUser($first_name, $last_name, $new_username);
                 User::updateUserById($id, $new_user);

                 return response()->json([User::getUserById($id)])
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }

    public function apiUpdateUserByUsername(Request $request, $username){
        try {
            $user = User::where('username', $username)->first();
            if(is_null($user)) {
                return response()->json()
                    ->setStatusCode(Response::HTTP_NOT_FOUND, Response::$statusTexts[Response::HTTP_NOT_FOUND]);
            }
            else {
                $first_name = request('first_name', $user->first_name);
                $last_name = request('last_name', $user->last_name);
                $new_username = request('username', $user->username);

                $new_user = User::createUser($first_name, $last_name, $new_username);
                User::updateUserByusername($username, $new_user);

                return response()->json([User::getUserByUsername($new_username)])
                    ->setStatusCode(Response::HTTP_OK, Response::$statusTexts[Response::HTTP_OK]);
            }
        } catch(Exception $exception) {
            return response()->json()
                ->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR, Response::$statusTextsn[Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }


    private function isAnyNullOrEmty(array $array){
        foreach ($array as $item) {
            if(is_null($item) || empty($item)) {
                return true;
            }
        }
        return false;
    }
}