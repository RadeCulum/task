<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';

    protected $fillable = ['nickname', 'text', 'blog_id'];

    public static  function getCommentById($id){
        return Comments::where('id', $id)->first();
    }

    public static function  createComment($nickname, $text, $blog_id){
        $comment = new Comments();

        $comment->nickname  = $nickname;
        $comment->text = $text;
        $comment->blog_id = $blog_id;

        return $comment;
    }

    public static function  createAdnSaveComment($nickname, $text, $blog_id){
        $comment = new Comments();

        $comment->nickname  = $nickname;
        $comment->text = $text;
        $comment->blog_id = $blog_id;

        $comment->save();

        return $comment;
    }

    public static function  deleteCommentById($id){
        Comments::where('id', $id)->first()->delete();
    }

    public static  function updateCommentById($id, $newComment){
        DB::table('comments')
            ->where('id', $id)
            ->update(array(
                'nickname' => $newComment->nickname,
                'text' => $newComment->text,
                'blog_id' => $newComment->blog_id
            ));
    }
}
