<?php

namespace Tests\Feature;

use App\Blog;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogTest extends TestCase
{
    public function testIndex(){
        $response = $this->json('GET','api/blogs');

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertJson(json_encode(Blog::all()),($response->getContent()));

    }
}
