<?php

namespace Tests\Feature;

use App\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Response;

class UserTest extends TestCase
{
    public function testIndex(){
        $response = $this->json('GET','api/users');

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertJson(json_encode(User::all()),($response->getContent()));

    }

    public function testGetUserById(){
        $user = User::getUserByUsername('TestUser');
        if(is_null($user)){
            $user = User::createAndSaveUser('Testo', 'Testic', 'TestUser');
            $deleUserAfterTest = true;
        } else {
            $deleUserAfterTest = false;
        }

        $response = $this->json('GET', "api/user/id/{$user->id}");

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertJson(json_encode(User::getUserById($user->id)),($response->getContent()));

        if($deleUserAfterTest){
            User::deleteUserById($user->id);
        }

        $response = $this->json('GET', "api/user/id/{$user->id}");

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->status());
        $this->assertJson(json_encode(''),($response->getContent()));

    }

    public function testGetUserByUsername(){
        $user = User::getUserByUsername('TestUser');
        if(is_null($user)){
            $user = User::createAndSaveUser('Testo', 'Testic', 'TestUser');
            $deleUserAfterTest = true;
        } else {
            $deleUserAfterTest = false;
        }

        $response = $this->json('GET', "api/user/username/{$user->username}");

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertJson(json_encode(User::getUserById($user->id)),($response->getContent()));

        if($deleUserAfterTest){
            User::deleteUserById($user->id);
        }

        $response = $this->json('GET', "api/user/username/{$user->username}");

        $this->assertEquals(Response::HTTP_NOT_FOUND, $response->status());
        $this->assertJson(json_encode(''),($response->getContent()));

    }

    public function createUser(){
        $user = User::getUserByUsername('TestUser');

        if(is_null($user)){
            $deleUserAfterTest = true;

            $response = $this->json('POST', "api/user", [
                'first_name' => '',
                'last_name' => 'Testic',
                'username' => 'TestUser'
            ]);
            $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->status());
            $this->assertJson(json_encode(''),($response->getContent()));

            $response = $this->json('POST', "api/user", [
                'first_name' => 'Testo',
                'username' => 'TestUser'
            ]);
            $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->status());
            $this->assertJson(json_encode(''),($response->getContent()));

            $response = $this->json('POST', "api/user", [
                'first_name' => 'Testo',
                'last_name' => 'Testic',
                'username' => 'TestUser'
                    ]);
            $this->assertEquals(Response::HTTP_OK, $response->status());
            $this->assertJson(json_encode(User::getUserByUsername($user->username)),($response->getContent()));

            $response = $this->json('POST', "api/user", [
                'first_name' => 'Testo',
                'last_name' => 'Testic',
                'username' => 'TestUser'
            ]);
            $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->status());
            $this->assertJson(json_encode(''),($response->getContent()));
        }

        if($deleUserAfterTest){
            User::deleteUserById($user->id);
        }
    }
}
