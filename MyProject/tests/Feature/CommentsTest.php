<?php

namespace Tests\Feature;

use App\Comments;
use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentsTest extends TestCase
{
    public function testIndex(){
        $response = $this->json('GET','api/comments');

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertJson(json_encode(Comments::all()),($response->getContent()));

    }
}
