<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />

    <title>Delete user</title>

</head>
<body>

<h1>Delete User</h1>

<div class="container">

    <form method="POST" action="/user/delete">

        {{ csrf_field() }}

        <div>
            <label >Username</label>
            <input type="text" name="username" placeholder="Username">

        </div>

        <div>

            <input type="submit" value="Delete">

        </div>

    </form>

</div>

</body>
</html>