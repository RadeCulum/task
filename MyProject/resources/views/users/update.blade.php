<!DOCTYPE html>
<html>
<head>
    <title>Update user</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />


</head>
<body>

<h1>Update User</h1>

<div class="container">

    <form method="POST" action="/user/update">

        {{ csrf_field() }}

        <div>
            <label >Old username</label>
            <input type="text" name="old_username" placeholder="Old username">

            <label >New first name</label>
            <input type="text" name="first_name" placeholder="First name">

            <label >New last name</label>
            <input type="text" name="last_name" placeholder="Last name">

            <label >New username</label>
            <input type="text" name="new_username" placeholder="New username">

        </div>

        <div>

            <input type="submit" value="Update">

        </div>

    </form>

</div>

</body>
</html>