<!DOCTYPE html>
<html>
    <head>
        <title>Users</title>

        <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Username</th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user-> first_name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->username}}</td>
                </tr>

            @endforeach

        </table>




        <form method="GET" action="user/add">
            <br/>
            <div>
                <input type="submit" value="Add new user">
            </div>
        </form>

        <form method="GET" action="user/update">
            <br/>
            <div>
                <input type="submit" value="Update user">
            </div>
        </form>

        <form method="GET" action="user/delete">
            <br/>
            <div>
                <input type="submit" value="Delete user">
            </div>
        </form>
    </body>
</html>