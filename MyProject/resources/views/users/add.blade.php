<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{ asset('css/style.css') }}" media="all" rel="stylesheet" type="text/css" />

    <title>Add user</title>

</head>
<body>

    <h1>Add New User</h1>

    <div class="container">

        <form method="POST" action="/user/add">

            {{ csrf_field() }}
            <div>
                <label >First name</label>
                <input type="text" name="first_name" id = "first_name" placeholder="First name">

                <label >Last name</label>
                <input type="text" name="last_name" id = "last_name" placeholder="Last name">

                <label >Username</label>
                <input type="text" name="username" id = "username" placeholder="Username">

            </div>

            <div>

                <input type="submit" value="Add">


            </div>

        </form>
    </div>

</body>
</html>